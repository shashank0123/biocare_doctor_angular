import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ListComponent } from './appointments/list/list.component';
import { ViewComponent } from './appointments/view/view.component';
import { AddComponent } from './appointments/add/add.component';
import { EditComponent } from './appointments/edit/edit.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppheaderComponent } from './appheader/appheader.component';
import { AppmenuComponent } from './appmenu/appmenu.component';
import { AppfooterComponent } from './appfooter/appfooter.component';
import { PatientslistComponent } from './patients/patientslist/patientslist.component';
import { PatientCreateComponent } from './patients/patient-create/patient-create.component';
import { PatientUpdateComponent } from './patients/patient-update/patient-update.component';
import { PatientViewComponent } from './patients/patient-view/patient-view.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListComponent,
    ViewComponent,
    AddComponent,
    EditComponent,
    DashboardComponent,
    AppheaderComponent,
    AppmenuComponent,
    AppfooterComponent,
    PatientslistComponent,
    PatientCreateComponent,
    PatientUpdateComponent,
    PatientViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
