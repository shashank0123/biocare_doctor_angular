import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ListComponent } from './appointments/list/list.component';
import { ViewComponent } from './appointments/view/view.component';
import { PatientViewComponent } from './patients/patient-view/patient-view.component';
import { PatientslistComponent } from './patients/patientslist/patientslist.component';



const routes: Routes = [
	

    {
    path: '',
    component: LoginComponent  
  },

{
    path: 'dashboard',
    component: DashboardComponent  
  },

  {
    path: 'appointments/list',
    component: ListComponent  
  },

  {
    path: 'appointment/view',
    component: ViewComponent  
  },

  {
    path: 'patients/list',
    component: PatientslistComponent  
  },

  {
    path: 'patient/view',
    component: PatientViewComponent  
  },

  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
